# About

Coordination repository for the Variable Key Symmetric Encryption algorithm.

# Credits

The algorithm was developed under supervision of Prof. B Dinesh Rao, a faculty of School of Information Sciences, Manipal University.
Contributors include:

* Aurabindo Jayamohanan
* Piyush Itankar
* Namrata Singh

# Working Group Activities

This is a public repository. Anyone can sumbit proposals and take part in discussions. Create a new issue with necessary details.
